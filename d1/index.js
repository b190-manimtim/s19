// console.log("Hello, B190!");

// if statement - executes a statement if a specified condition is true
// can stand alone without the else statement

let numA = -1;

if (numA < 0) {
    console.log("Hello");
};

console.log(numA < 0);

if (numA > 0) {
    console.log("This statement will not be printed");
};

let city = "New York";
if (city === "New York") {
    console.log("Welcome to New York City");
};

// else if clause
// executes a statement if previous conditions are false and if the specified condition is true. It is optional and can be added to capture additional conditions to change the flow of a program

let numB = 1;
if (numA > 0) {
    console.log("Hello!");
} else if (numB > 0) {
    console.log("World");
};

if (numA < 0) {
    console.log("Hello!");
} else if (numB > 0) {
    console.log("World");
};

city = "Tokyo";
if (city === "New York") {
    console.log("Welcome to New York City!");
} else if (city === "Tokyo") {
    console.log("Welcome to Tokyo!");
};

// else statement
// executes a statement if all other conditions are false.It is Optional and can be added to capture any other result to change the flow of a program

if (numA > 0) {
    console.log("Hello");
} else if (numB === 0) {
    console.log("World");
} else {
    console.log("Again");
};

// Another example for else statement

// let age = prompt("Enter your age: ")
// if (age <= 18) {
//     console.log("Not allowed to drink!");
// } else {
//     console.log("Matanda kana, shot na!");
// };

// Mini Activity

// function getHeight() {
//     let height = prompt("Enter your height: ")
//     if (height < 150) {
//         console.log("Did not passed the minimum height requirement");
//     } else if (height >= 150) {
//         console.log("Passed the minimum height requirements");
//     } else {
//         console.log("Not a height")
//     }
// };
// getHeight();

let message = "No Message.";
console.log(message);

function determineTyphonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return "not a typhoon yet"
    } else if (windSpeed <= 61) {
        return "tropical depression detected"
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return "Tropical Storm detected"
    } else if (windSpeed >= 89 && windSpeed <= 117) {
        return "Severe tropical storm detected"
    } else {
        return "Typhoon Detected"
    }
};
message = determineTyphonIntensity(69)
console.log(message);

if (message == "Tropical Storm detected") {
    console.warn(message);
}

// Truthy and Falsy values
// In javascript, a truthy value is a value that is considered true when encountered in a Boolean context. Unless, defined otherwise.
// Falsy Values/exception for truthy

// Truthy example
if (true) {
    console.log("Truthy!");
};

if (1) {
    console.log("Truthy!");
};
if ([]) {
    console.log("Truthy");
};

// Falsy examples
if (false) {
    console.log("Falsy");
};

if (0) {
    console.log("Falsy");
};

if (undefined) {
    console.log("Falsy");
};

// Conditional (Ternary) Operator
// Takes in three operands. 
// 1. Condition
// 2. expression to execute if the condition is truthy
// 3. expression to execute if the condition is falsy

// Single statement execution

let ternaryResult = (1 < 180) ? "Statement is True" : "Statement is False";
console.log("Result of ternary operator: " + ternaryResult);

let name;

function isOfLegalAge() {
    name = 'John';
    return ' You are of the legal age limit';
};

function isUnderAge() {
    name = "Jane";
    return "You are under the age limit";
};

// let age = parseInt(prompt("What is your age?"));
// console.log(age);
// console.log(typeof age);

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary operator in function: " + legalAge + ', ' + name);

// Switch Statement

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day)
// switch (day) {
//     case 'monday':
//         console.log("The color of the day is red");
//         break;
//     case 'tuesday':
//         console.log("The color of the day is orange");
//         break;
//     case 'wednesday':
//         console.log("The color of the day is yellow");
//         break;
//     case 'thursday':
//         console.log("The color of the day is green");
//         break;
//     case 'friday':
//         console.log("The color of the day is blue");
//         break;
//     case 'saturday':
//         console.log("The color of the day is indigo");
//         break;
//     case 'sunday':
//         console.log("The color of the day is violet");
//         break;
//         // case ('saturday' || 'sabado'):
//         //     console.log("The color of the day is indigo");
//         //     break;
//     default:
//         console.log("Please input a valid day!");
//         break;
// }

// Try-Catch-Finally Statement
function showIntensityAlert(windSpeed) {
    try {
        alerat(determineTyphonIntensity(windSpeed))
    } catch (error) {
        console.log(error)
        console.warn(error.message)
    } finally {
        alert("Intensity updates will shows new alert");
    }
};
showIntensityAlert(56);