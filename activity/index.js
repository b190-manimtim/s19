let username;
let password;
let role;

function loginDetails(username, password, role) {
    username = prompt("Enter your username: ");
    password = prompt("Enter your password: ");
    role = prompt("Enter your role: ")

    if (username == null || username == "" || password == null || password == "" || role == null || role == "") {
        alert("Input must not be empty");
    } else {
        switch (role) {
            case 'teacher':
                alert("Thank you for logging in, teacher");
                break;
            case 'student':
                alert("Welcome to the class portal, student")
                break;
            case 'admin':
                alert("Welcome to the class portal, admin")
                break;
            default:
                console.log("Role out of range");
                break;
        }
    }


};
loginDetails();

function checkAverage(grade1, grade2, grade3, grade4) {
    let average = (grade1 + grade2 + grade3 + grade4) / 4;
    average = Math.round(average);

    if (average <= 74) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is F");
    } else if (average <= 79) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is D");
    } else if (average <= 84) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is C");
    } else if (average <= 89) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is B");
    } else if (average <= 95) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is A");
    } else if (average <= 96) {
        console.log("Hello, Student, your average is: " + average + "." + " The letter equivalent is A+");
    }

}